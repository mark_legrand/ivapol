import { writable } from 'svelte/store';

export const siteData = writable({
    telephone: '+44(0) 740 534 0757',
    exp: new Date().getFullYear() - 2016
})